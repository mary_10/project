import { element, by, ElementFinder } from 'protractor';

export class QuestionComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-question div table .btn-danger'));
  title = element.all(by.css('jhi-question div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class QuestionUpdatePage {
  pageTitle = element(by.id('jhi-question-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  questionNameInput = element(by.id('field_questionName'));
  questionTextInput = element(by.id('field_questionText'));
  mandatoryInput = element(by.id('field_mandatory'));
  questionTypeSelect = element(by.id('field_questionType'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setQuestionNameInput(questionName: string): Promise<void> {
    await this.questionNameInput.sendKeys(questionName);
  }

  async getQuestionNameInput(): Promise<string> {
    return await this.questionNameInput.getAttribute('value');
  }

  async setQuestionTextInput(questionText: string): Promise<void> {
    await this.questionTextInput.sendKeys(questionText);
  }

  async getQuestionTextInput(): Promise<string> {
    return await this.questionTextInput.getAttribute('value');
  }

  getMandatoryInput(): ElementFinder {
    return this.mandatoryInput;
  }

  async setQuestionTypeSelect(questionType: string): Promise<void> {
    await this.questionTypeSelect.sendKeys(questionType);
  }

  async getQuestionTypeSelect(): Promise<string> {
    return await this.questionTypeSelect.element(by.css('option:checked')).getText();
  }

  async questionTypeSelectLastOption(): Promise<void> {
    await this.questionTypeSelect.all(by.tagName('option')).last().click();
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class QuestionDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-question-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-question'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
