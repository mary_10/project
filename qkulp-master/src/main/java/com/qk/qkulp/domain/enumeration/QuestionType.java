package com.qk.qkulp.domain.enumeration;

/**
 * The QuestionType enumeration.
 */
public enum QuestionType {
    BOOLEAN, SINGLE_CHOICE, MULTIPLE_CHOICE, TEXT
}
