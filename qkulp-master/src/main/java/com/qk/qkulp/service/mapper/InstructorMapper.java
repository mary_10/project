package com.qk.qkulp.service.mapper;


import com.qk.qkulp.domain.*;
import com.qk.qkulp.service.dto.InstructorDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Question} and its DTO {@link QuestionDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface InstructorMapper extends EntityMapper<InstructorDTO, Instructor> {



    default Instructor fromId(Long id) {
        if (id == null) {
            return null;
        }
        Instructor instructor = new Instructor();
        instructor.setId(id);
        return instructor;
    }
}