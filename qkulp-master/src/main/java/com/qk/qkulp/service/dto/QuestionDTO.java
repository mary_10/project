package com.qk.qkulp.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import com.qk.qkulp.domain.enumeration.QuestionType;

/**
 * A DTO for the {@link com.qk.qkulp.domain.Question} entity.
 */
public class QuestionDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 30)
    private String questionName;

    @NotNull
    @Size(max = 400)
    private String questionText;

    @NotNull
    private Boolean mandatory;

    @NotNull
    private QuestionType questionType;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestionName() {
        return questionName;
    }

    public void setQuestionName(String questionName) {
        this.questionName = questionName;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof QuestionDTO)) {
            return false;
        }

        return id != null && id.equals(((QuestionDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "QuestionDTO{" +
            "id=" + getId() +
            ", questionName='" + getQuestionName() + "'" +
            ", questionText='" + getQuestionText() + "'" +
            ", mandatory='" + isMandatory() + "'" +
            ", questionType='" + getQuestionType() + "'" +
            "}";
    }
}
