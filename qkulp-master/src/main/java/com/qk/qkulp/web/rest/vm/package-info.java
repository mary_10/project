/**
 * View Models used by Spring MVC REST controllers.
 */
package com.qk.qkulp.web.rest.vm;
