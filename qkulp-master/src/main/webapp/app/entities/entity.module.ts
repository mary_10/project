import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'question',
        loadChildren: () => import('./question/question.module').then(m => m.QkulpQuestionModule),
      },
      {
        path: 'course',
        loadChildren: () => import('./course/course.module').then(m => m.QkulpCourseModule),
      },
     
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class QkulpEntityModule {}
