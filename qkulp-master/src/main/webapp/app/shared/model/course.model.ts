export interface ICourse {
  id?: number;
  name?: string;
  hours?: number;
}

export class Course implements ICourse {
  constructor(public id?: number, public name?: string, public hours?: number) {}
}
