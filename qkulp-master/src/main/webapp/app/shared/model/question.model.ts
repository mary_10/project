import { QuestionType } from 'app/shared/model/enumerations/question-type.model';

export interface IQuestion {
  id?: number;
  questionName?: string;
  questionText?: string;
  mandatory?: boolean;
  questionType?: QuestionType;
}

export class Question implements IQuestion {
  constructor(
    public id?: number,
    public questionName?: string,
    public questionText?: string,
    public mandatory?: boolean,
    public questionType?: QuestionType
  ) {
    this.mandatory = this.mandatory || false;
  }
}
