export const enum QuestionType {
  BOOLEAN = 'BOOLEAN',

  SINGLE_CHOICE = 'SINGLE_CHOICE',

  MULTIPLE_CHOICE = 'MULTIPLE_CHOICE',

  TEXT = 'TEXT',
}
